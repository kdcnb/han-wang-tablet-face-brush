package com.arcsoft.arcfacedemo;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

/**
 * Created by jzh on 2024/1/9.
 */
public class FaceApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
