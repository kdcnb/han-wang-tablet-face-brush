package com.arcsoft.arcfacedemo.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.Camera;
import android.hardware.usb.UsbDevice;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.Pair;
import android.view.*;
import android.widget.*;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.UiThread;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arcsoft.arcfacedemo.BuildConfig;
import com.arcsoft.arcfacedemo.R;
import com.arcsoft.arcfacedemo.common.Constants;
import com.arcsoft.arcfacedemo.faceserver.CompareResult;
import com.arcsoft.arcfacedemo.faceserver.FaceServer;
import com.arcsoft.arcfacedemo.model.DrawInfo;
import com.arcsoft.arcfacedemo.model.FacePreviewInfo;
import com.arcsoft.arcfacedemo.model.PaidRecordEntity;
import com.arcsoft.arcfacedemo.model.UserEntity;
import com.arcsoft.arcfacedemo.util.ConfigUtil;
import com.arcsoft.arcfacedemo.util.DrawHelper;
import com.arcsoft.arcfacedemo.util.FormatUtil;
import com.arcsoft.arcfacedemo.util.face.FaceHelper;
import com.arcsoft.arcfacedemo.util.face.FaceListener;
import com.arcsoft.arcfacedemo.util.face.LivenessType;
import com.arcsoft.arcfacedemo.util.face.RecognizeColor;
import com.arcsoft.arcfacedemo.util.face.RequestFeatureStatus;
import com.arcsoft.arcfacedemo.util.face.RequestLivenessStatus;
import com.arcsoft.arcfacedemo.widget.FaceRectView;
import com.arcsoft.arcfacedemo.widget.FaceSearchResultAdapter;
import com.arcsoft.face.AgeInfo;
import com.arcsoft.face.ErrorInfo;
import com.arcsoft.face.FaceEngine;
import com.arcsoft.face.FaceFeature;
import com.arcsoft.face.FaceInfo;
import com.arcsoft.face.GenderInfo;
import com.arcsoft.face.LivenessInfo;
import com.arcsoft.face.enums.DetectFaceOrientPriority;
import com.arcsoft.face.enums.DetectMode;
import com.arcsoft.imageutil.ArcSoftImageFormat;
import com.arcsoft.imageutil.ArcSoftImageUtil;
import com.bumptech.glide.Glide;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import com.serenegiant.usb.DeviceFilter;
import com.serenegiant.usb.IFrameCallback;
import com.serenegiant.usb.USBMonitor;
import com.serenegiant.usb.UVCCamera;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * 1.活体检测使用IR摄像头数据，其他都使用RGB摄像头数据
 * <p>
 * 2.本界面仅实现IR数据和RGB数据预览大小相同且画面十分接近的情况（RGB数据和IR数据无旋转、镜像的关系）的活体检测，
 * <p>
 * 3.若IR数据和RGB数据预览大小不同或两者成像的人脸位置差别很大，需要自己实现人脸框的调整方案。
 * <p>
 * 4.由于不同的厂商对IR Camera和RGB Camera的CameraId设置可能会有所不同，开发者可能需要根据实际情况修改
 * {@link TestActivity#cameraRgbId}和
 * {@link TestActivity#cameraIrId}的值
 * <p>
 * 5.由于一般情况下android设备的前置摄像头，即cameraId为{@link Camera.CameraInfo#CAMERA_FACING_FRONT}的摄像头在打开后会自动被镜像预览。
 * 为了便于开发者们更直观地了解两个摄像头成像的关系，实现人脸框的调整方案，本demo对cameraId为{@link Camera.CameraInfo#CAMERA_FACING_FRONT}
 * 的预览画面做了再次镜像的处理，也就是恢复为原画面。
 */
public class TestActivity extends BaseActivity implements ViewTreeObserver.OnGlobalLayoutListener {
    private static final String TAG = "IrRegisterAndRecognize";
    private static final int MAX_DETECT_NUM = 1;
    /**
     * 当FR成功，活体未成功时，FR等待活体的时间
     */
    private static final int WAIT_LIVENESS_INTERVAL = 5;
    /**
     * 失败重试间隔时间（ms）
     */
    private static final long FAIL_RETRY_INTERVAL = 10;
    /**
     * 出错重试最大次数
     */
    private static final int MAX_RETRY_TIME = 3;

    private DrawHelper drawHelperRgb;
    private DrawHelper drawHelperIr;


    /**
     * RGB摄像头和IR摄像头的ID，若和实际不符，需要修改以下两个值。
     * 同时，可能需要修改默认的VIDEO模式人脸检测角度
     */
    private Integer cameraRgbId = Camera.CameraInfo.CAMERA_FACING_BACK;
    private Integer cameraIrId = Camera.CameraInfo.CAMERA_FACING_FRONT;

    private FaceEngine ftEngine;
    private FaceEngine frEngine;
    private FaceEngine flEngine;

    private int ftInitCode = -1;
    private int frInitCode = -1;
    private int flInitCode = -1;

    private FaceHelper faceHelperIr;
    private List<CompareResult> compareResultList;
    /**
     * 活体检测的开关
     */
    private boolean livenessDetect = true;

    /**
     * 注册人脸状态码，准备注册
     */
    private static final int REGISTER_STATUS_READY = 0;
    /**
     * 注册人脸状态码，注册中
     */
    private static final int REGISTER_STATUS_PROCESSING = 1;
    /**
     * 注册人脸状态码，注册结束（无论成功失败）
     */
    private static final int REGISTER_STATUS_DONE = 2;

    private int registerStatus = REGISTER_STATUS_DONE;

    private MediaPlayer mediaPlayer;


    /**
     * 用于记录人脸识别相关状态
     */
    private ConcurrentHashMap<Integer, Integer> requestFeatureStatusMap = new ConcurrentHashMap<>();
    /**
     * 用于记录人脸特征提取出错重试次数
     */
    private ConcurrentHashMap<Integer, Integer> extractErrorRetryMap = new ConcurrentHashMap<>();
    /**
     * 用于存储活体值
     */
    private ConcurrentHashMap<Integer, Integer> livenessMap = new ConcurrentHashMap<>();
    /**
     * 用于存储活体检测出错重试次数
     */
    private ConcurrentHashMap<Integer, Integer> livenessErrorRetryMap = new ConcurrentHashMap<>();

    private CompositeDisposable getFeatureDelayedDisposables = new CompositeDisposable();
    private CompositeDisposable delayFaceTaskCompositeDisposable = new CompositeDisposable();
    /**
     * 相机预览显示的控件，可为SurfaceView或TextureView
     */
    private SurfaceView previewViewRgb;
    private SurfaceView previewViewIr;
    /**
     * 绘制人脸框的控件
     */
    private FaceRectView faceRectView;

    private ImageView im_pay_dialog;
    private TextView tv_balance_dialog;
    private TextView tv_name_dialog;
    private LinearLayout ll_pay_dialog;
    private TextView tv_item_name;
    private ImageView iv_item_head_img;

    private LinearLayout dual_camera_view_person;
    private TextView tv_hour;
    private TextView tv_week;
    private TextView tv_year;


    private static final int ACTION_REQUEST_PERMISSIONS = 0x001;

    /**
     * 识别阈值
     */
    private static final float SIMILAR_THRESHOLD = 0.8F;

    /**
     * 所需的所有权限信息
     */
    private static final String[] NEEDED_PERMISSIONS = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private volatile byte[] rgbData;
    private volatile byte[] irData;
    private final Object mSync = new Object();


    //---------------------------------------------uvc相关start--------------

    private boolean isActive, isNirActive, isPreview, isNirPreview;
    private USBMonitor mUSBMonitor;
    public UVCCamera mUVCCamera;
    private UVCCamera mNirUVCCamera;


    //---------------------------------------------uvc相关end-----------------

    /*[#######################################################################################]*/

    private static final int HANDLER_YM_FACE_ANALYZE_SUCCESS = 1;
    private static final int HANDLER_PAYING_FAILED_STATE = 2;
    private static final int HANDLER_PAYING_AGAIN_FAILED_STATE = 3;

    private static final int HANDLER_PAYING_SUCCESS_STATE = 4;
    private static final int HANDLER_PAYING_SUCCESS_RESET_STATE = 5;
    private static final int HANDLER_PAYING_FAIL_RESET_STATE = 6;
    private static final int HANDLER_ANALYZING_RESET_STATE = 9;
    private static final int HANDLER_PAYING_CONFIRMED_STATE = 7;/*[用户确认支付]*/
    private static final int HANDLER_SLUICE_GATE_RESET_STATE = 10;
    private static final int HANDLER_NETWORK_CHANGE = 11;
    private static final int HANDLER_NETWORK_RECOVER = 12;
    private static final int HANDLER_OFFLINE_COST_UPLOAD = 13;
    // 是否开启人脸识别
    private volatile boolean isAnalyzing = true;
    /*[人脸识别 start]*/
    private UserEntity payUser;
    private PaidRecordEntity recordEntity=new PaidRecordEntity();
    private List<PaidRecordEntity> recordEntityList=new ArrayList<>();

    public void setAnalyzing(boolean analyzing) {
        System.err.println(analyzing);
        isAnalyzing = analyzing;
        // todo
    }
    //显示支付完成
    @SuppressLint("SetTextI18n")
    public void showPaySuccessDialog() {
        ll_pay_dialog.setVisibility(View.VISIBLE);
        tv_balance_dialog.setVisibility(View.VISIBLE);
        tv_name_dialog.setText(payUser.getName());
        tv_balance_dialog.setText(payUser.getCard());
        im_pay_dialog.setImageResource(R.drawable.main_pay_suree_bg);
        AnimationDrawable animationDrawable = (AnimationDrawable) im_pay_dialog.getDrawable();
        animationDrawable.start();
        mHandler.sendEmptyMessageDelayed(HANDLER_PAYING_SUCCESS_RESET_STATE, 1000);
        playMediaPlayer(1);
    }
    //showPayAgainSuccessDialog
    @SuppressLint("SetTextI18n")
    public void showPayAgainSuccessDialog() {
        ll_pay_dialog.setVisibility(View.VISIBLE);
        tv_balance_dialog.setVisibility(View.VISIBLE);
        tv_name_dialog.setText(payUser.getName());
        tv_balance_dialog.setText(payUser.getCard());
        im_pay_dialog.setImageResource(R.drawable.main_pay_suree_bg);
        AnimationDrawable animationDrawable = (AnimationDrawable) im_pay_dialog.getDrawable();
        animationDrawable.start();
        mHandler.sendEmptyMessageDelayed(HANDLER_PAYING_SUCCESS_RESET_STATE, 1000);
        playMediaPlayer(4);
    }
    @SuppressLint("SetTextI18n")
    @UiThread
    private void updateMainDateTime() {
        Calendar calendar = Calendar.getInstance();
        //
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        tv_hour.setText(String.format(Locale.getDefault(),
                "%02d:%02d", hour, minute
        ));
        //
        int weekIndex = calendar.get(Calendar.DAY_OF_WEEK);
        final String[] dayOfWeekNames = getResources().getStringArray(
                R.array.day_of_week_names
        );
        tv_week.setText(dayOfWeekNames[weekIndex]);
        //
        String dateValue = String.format(Locale.getDefault(),
                "%04d/%02d/%02d",
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH) + 1,
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        tv_year.setText(dateValue);
    }


    private final Runnable mUpdateTimeAction = new Runnable() {
        @Override
        public void run() {
            while (!TestActivity.this.isDestroyed()) {
                //回到主线程更新UI
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        updateMainDateTime();
                    }
                });
                try {
                    TimeUnit.SECONDS.sleep(10L);
                } catch (InterruptedException e) {
                    //do noting.
                }
            }
        }
    };

    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler(new Handler.Callback() {
        @SuppressLint("SetTextI18n")
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case HANDLER_YM_FACE_ANALYZE_SUCCESS:
                    Log.i(TAG, "识别成功");
                    /*[如果已经识别成功，则过虑掉当前结果]*/
                    if (!isAnalyzing) {
                        return false;
                    }
                    setAnalyzing(false);

                    faceRectView.clearFaceInfo();
                    if (recordEntityList.size() > 0) {
                        boolean isAgain = false;
                        for (PaidRecordEntity entity : recordEntityList) {
                            if (entity.getName().equals(payUser.getName())){
                                isAgain = true;
                                break;
                            }
                        }
                        if (isAgain){
                            showPayAgainSuccessDialog();
                            break;
                        }
                    }
                    showPaySuccessDialog();
                    break;
                case HANDLER_PAYING_SUCCESS_STATE:
                    //支付完成
//                    mHandler.sendEmptyMessageDelayed(HANDLER_SLUICE_GATE_RESET_STATE, 2000);
                    showPayAgainSuccessDialog();
                    break;
                case HANDLER_PAYING_CONFIRMED_STATE:
//                    if (requestApplication().userData().getOffline()) {
//                        setOfflineCostSaveData(payUser);
//                    } else {
//                        setCostSaveData(payUser);
//                    }
                    break;
                case HANDLER_PAYING_SUCCESS_RESET_STATE:
                    //setRelay(0);
                    ll_pay_dialog.setVisibility(View.GONE);
                    tv_balance_dialog.setVisibility(View.GONE);
                    recordEntity.setLastTime(System.currentTimeMillis());
                    recordEntity.setCard(payUser.getCard());
                    recordEntity.setName(payUser.getName());

                    recordEntityList.add(recordEntity);
                    setAnalyzing(true);
                    break;
                case HANDLER_PAYING_FAIL_RESET_STATE:
//                    setRelay(0);
                    break;

                case HANDLER_ANALYZING_RESET_STATE:
                    break;
                case HANDLER_SLUICE_GATE_RESET_STATE:
//                    GPIOManager.getInstance(requestActivity()).pullDownRelay();
                    //setRelay(0);
//                    userRecord.setUserName("");
                    break;

                case HANDLER_PAYING_AGAIN_FAILED_STATE:
                    break;
                case HANDLER_PAYING_FAILED_STATE:
                    break;
                case HANDLER_NETWORK_CHANGE:
                    Log.d("NETWORK_CHECK", "-- start to HANDLER_NETWORK_CHANGE...");
//                    showOfflineDialog();
                    break;
                case HANDLER_NETWORK_RECOVER:
                    Log.d("NETWORK_CHECK", "-- recover network...");
                    //
                    mHandler.sendEmptyMessageDelayed(HANDLER_OFFLINE_COST_UPLOAD, 1000);
                    break;
                case HANDLER_OFFLINE_COST_UPLOAD:
                    Log.d("NETWORK_CHECK", "-- start to upload offline cost...");
                    break;
                default:
                    break;
            }
            return true;
        }
    });


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_and_recognize_ir);
        //保持亮屏
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

//        openDebug(BuildConfig.DEBUG);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WindowManager.LayoutParams attributes = getWindow().getAttributes();
            attributes.systemUiVisibility = View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            getWindow().setAttributes(attributes);
        }

        // Activity启动后就锁定为启动时的方向
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        //本地人脸库初始化
        FaceServer.getInstance().init(this);

        initMyView();
        //双目开补光灯 -单目低亮
//        WriteLightNode(1);
        //开始定位动画
        View viewById = findViewById(R.id.main_layout_address);
        AnimationDrawable animationDrawable = (AnimationDrawable) viewById.getBackground();
        animationDrawable.start();
        //uvc相关-----------------
        mUSBMonitor = new USBMonitor(this, mOnDeviceConnectListener);
        mUSBMonitor.register();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            connectCamera();
        }

    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void connectCamera() {

        final List<DeviceFilter> filter = DeviceFilter.getDeviceFilters(this, R.xml.device_filter);
        List<UsbDevice> deviceList = mUSBMonitor.getDeviceList(filter);


        UsbDevice rgbCameraDevice = null;
        UsbDevice nirCameraDevice = null;
        for (UsbDevice device : deviceList) {
            String deviceName = device.getDeviceName();
            int deviceId = device.getDeviceId();

            String ss = deviceName + "[" + deviceId + "]" + device.getProductName();
            Log.d(TAG, "--- usb device is: " + ss);

            String mProductName = device.getProductName();

            if ("/dev/bus/usb/001/028".equalsIgnoreCase(deviceName)) {
                rgbCameraDevice = device;
                Log.d(TAG, "--- connect RGB Camera...");
                mUSBMonitor.requestPermission(rgbCameraDevice);

            } else if ("/dev/bus/usb/001/030".equalsIgnoreCase(deviceName)) {
                nirCameraDevice = device;
                Log.d(TAG, "--- connect NIR Camera...");
                mUSBMonitor.requestPermission(nirCameraDevice);

            }
        }
    }
    private final USBMonitor.OnDeviceConnectListener mOnDeviceConnectListener
            = new USBMonitor.OnDeviceConnectListener() {

        @Override
        public void onAttach(final UsbDevice device) {
            Log.e(TAG, "[USB_CAMERA] -- onAttach...");

        }

        @Override
        public void onDettach(UsbDevice usbDevice) {
            Log.e(TAG, "[USB_CAMERA] -- onDettach...");

        }

        @Override
        public void onConnect(final UsbDevice device,
                              final USBMonitor.UsbControlBlock ctrlBlock, final boolean createNew) {

            Log.e(TAG, "onConnect:");
            int devNum = ctrlBlock.getDevNum();
            String mProductName = ctrlBlock.getDeviceName();
            Log.e(TAG, "onConnect: mProductName is: " + mProductName);
            if (devNum == 28) {
                previewViewRgb.post(() -> {
                    final UVCCamera camera = new UVCCamera();
                    camera.open(ctrlBlock);
                    Log.d(TAG, "supportedSize:" + camera.getSupportedSize());
                    camera.setPreviewSize(UVCCamera.DEFAULT_PREVIEW_WIDTH, UVCCamera.DEFAULT_PREVIEW_HEIGHT, UVCCamera.PIXEL_FORMAT_NV21);

                    //todo 根据实际情况具体修改
//                    drawHelperRgb = new DrawHelper(UVCCamera.DEFAULT_PREVIEW_WIDTH, UVCCamera.DEFAULT_PREVIEW_HEIGHT, previewViewRgb.getWidth(),
//                            previewViewRgb.getHeight(), 0,
//                            0, false, false, false);
//                    if (faceHelperIr == null) {
//                        faceHelperIr = new FaceHelper.Builder()
//                                .ftEngine(ftEngine)
//                                .frEngine(frEngine)
//                                .flEngine(flEngine)
//                                .frQueueSize(MAX_DETECT_NUM)
//                                .flQueueSize(MAX_DETECT_NUM)
//                                .previewPair(new Pair<>(UVCCamera.DEFAULT_PREVIEW_WIDTH, UVCCamera.DEFAULT_PREVIEW_HEIGHT))
//                                .faceListener(faceListener)
//                                .trackedFaceCount(ConfigUtil.getTrackedFaceCount(TestActivity.this.getApplicationContext()))
//                                .build();
//                    }
                    isActive = true;
                    Log.d(TAG, "[USB_CAMERA] -- startPreview...");
                    camera.setPreviewDisplay(previewViewRgb.getHolder());
                    camera.startPreview();
                    camera.setFrameCallback(mIFrameCallback, UVCCamera.PIXEL_FORMAT_NV21);
                    //isPreview = true;
                    mUVCCamera = camera;
                });

            } else {
                //startNirCameraPreview(ctrlBlock);
                previewViewIr.post(() -> {
                    final UVCCamera camera = new UVCCamera();
                    camera.open(ctrlBlock);
                    Log.d(TAG, "supportedSize:" + camera.getSupportedSize());
                    camera.setPreviewSize(UVCCamera.DEFAULT_PREVIEW_WIDTH, UVCCamera.DEFAULT_PREVIEW_HEIGHT, UVCCamera.PIXEL_FORMAT_NV21);
                    //根据实际情况具体修改
//                    drawHelperIr = new DrawHelper(UVCCamera.DEFAULT_PREVIEW_WIDTH, UVCCamera.DEFAULT_PREVIEW_HEIGHT,
//                            previewViewIr.getWidth(), previewViewIr.getHeight(), 0,
//                            1, false, false, false);
                    isNirActive = true;
                    Log.d(TAG, "[USB_CAMERA] -- startNirPreview...");
                    camera.setPreviewDisplay(previewViewIr.getHolder());
                    camera.startPreview();
                    camera.setFrameCallback(mNirIFrameCallback, UVCCamera.PIXEL_FORMAT_NV21);
                    Log.d(TAG, "[USB_CAMERA] -- setNirFrameCallback...");
                    //isNirPreview = true;
                    mNirUVCCamera = camera;
                });
            }
        }
        private final IFrameCallback mIFrameCallback = new IFrameCallback() {
            @Override
            public void onFrame(ByteBuffer byteBuffer) {
                //Log.d(TAG, "[USB_CAMERA] -- mIFrameCallback, onFrame...");
//                boolean isAnalyzing = true;
                if (isPreview) {
                    Log.d(TAG, "[USB_CAMERA] -- mIFrameCallback, start analyzing");

                    byte[] data = new byte[byteBuffer.remaining()];
                    byteBuffer.get(data);
                    rgbData = data;
                    processPreviewData();
                    //Log.d(TAG, "[USB_CAMERA] -- mIFrameCallback, stop analyzing");
                }
            }
        };

        private final IFrameCallback mNirIFrameCallback = new IFrameCallback() {
            @Override
            public void onFrame(ByteBuffer byteBuffer) {
                //Log.d(TAG, "[USB_CAMERA] -- mNirIFrameCallback, onFrame...");

//                boolean isAnalyzing = true;

                if (isNirPreview) {
                    //Log.d(TAG, "[USB_CAMERA] -- mIFrameCallback, start nir analyzing");

                    byte[] data = new byte[byteBuffer.remaining()];
                    byteBuffer.get(data);

                    irData =data;
                    //Log.d(TAG, "[USB_CAMERA] -- mIFrameCallback, stop nir analyzing");
                }
            }
        };

        @Override
        public void onDisconnect(UsbDevice usbDevice, USBMonitor.UsbControlBlock usbControlBlock) {
            Log.d(TAG, "onDisconnect:");
            if (mUVCCamera != null) {

            }

            if (mNirUVCCamera != null) {

            }
        }

        @Override
        public void onCancel(UsbDevice usbDevice) {
            Log.d(TAG, "onCancel:");
        }

    };


    private void initMyView() {
        previewViewRgb = findViewById(R.id.dual_camera_texture_preview_rgb);
        //在布局结束后才做初始化操作
        previewViewRgb.getViewTreeObserver().addOnGlobalLayoutListener(this);
        previewViewIr = findViewById(R.id.dual_camera_texture_previewIr);
        faceRectView = findViewById(R.id.dual_camera_face_rect_view);
        im_pay_dialog = findViewById(R.id.main_pay_dialog_image);

        ll_pay_dialog = findViewById(R.id.main_pay_dialog_layout);
        tv_name_dialog = findViewById(R.id.main_dialog_name_text);
        tv_balance_dialog = findViewById(R.id.main_dialog_balance_text);

        tv_hour = findViewById(R.id.main_layout_hour_text);
        tv_week = findViewById(R.id.main_layout_week_text);
        tv_year = findViewById(R.id.main_layout_year_text);

        updateMainDateTime();
        new Thread(mUpdateTimeAction).start();
        //启动时间线程
        //iv_item_head_img,tv_item_name
        tv_item_name = findViewById(R.id.tv_item_name);
        iv_item_head_img = findViewById(R.id.iv_item_head_img);


        dual_camera_view_person = findViewById(R.id.dual_camera_recycler_view_person);
        compareResultList = new ArrayList<>();
    }


    /**
     * 初始化引擎
     */
    private void initEngine() {
        ftEngine = new FaceEngine();
        ftInitCode = ftEngine.init(this, DetectMode.ASF_DETECT_MODE_VIDEO, DetectFaceOrientPriority.ASF_OP_ALL_OUT,
                16, MAX_DETECT_NUM, FaceEngine.ASF_FACE_DETECT);

        frEngine = new FaceEngine();
        frInitCode = frEngine.init(this, DetectMode.ASF_DETECT_MODE_IMAGE, DetectFaceOrientPriority.ASF_OP_ALL_OUT,
                16, MAX_DETECT_NUM, FaceEngine.ASF_FACE_RECOGNITION);

        flEngine = new FaceEngine();
        flInitCode = flEngine.init(this, DetectMode.ASF_DETECT_MODE_IMAGE, DetectFaceOrientPriority.ASF_OP_ALL_OUT,
                16, MAX_DETECT_NUM, FaceEngine.ASF_IR_LIVENESS);

        Log.i(TAG, "initEngine:  init: " + ftInitCode);
        //main_layout_user_count
        TextView textView = findViewById(R.id.main_layout_user_count);
        textView.setText("当前模版数：" + FaceServer.getInstance().getFaceNumber(this));
        if (ftInitCode != ErrorInfo.MOK) {
            String error = getString(R.string.specific_engine_init_failed, "ftEngine", ftInitCode);
            Log.i(TAG, "initEngine: " + error);
            showToast(error);
        }
        if (frInitCode != ErrorInfo.MOK) {
            String error = getString(R.string.specific_engine_init_failed, "frEngine", ftInitCode);
            Log.i(TAG, "initEngine: " + error);
            showToast(error);
        }
        if (flInitCode != ErrorInfo.MOK) {
            String error = getString(R.string.specific_engine_init_failed, "flEngine", ftInitCode);
            Log.i(TAG, "initEngine: " + error);
            showToast(error);
        }
    }

    /**
     * 销毁引擎，faceHelperIr中可能会有特征提取耗时操作仍在执行，加锁防止crash
     */
    private void unInitEngine() {
        if (ftInitCode == ErrorInfo.MOK && ftEngine != null) {
            synchronized (ftEngine) {
                int ftUnInitCode = ftEngine.unInit();
                Log.i(TAG, "unInitEngine: " + ftUnInitCode);
            }
        }
        if (frInitCode == ErrorInfo.MOK && frEngine != null) {
            synchronized (frEngine) {
                int frUnInitCode = frEngine.unInit();
                Log.i(TAG, "unInitEngine: " + frUnInitCode);
            }
        }
        if (flInitCode == ErrorInfo.MOK && flEngine != null) {
            synchronized (flEngine) {
                int flUnInitCode = flEngine.unInit();
                Log.i(TAG, "unInitEngine: " + flUnInitCode);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {


        unInitEngine();

        if (getFeatureDelayedDisposables != null) {
            getFeatureDelayedDisposables.clear();
        }
        if (delayFaceTaskCompositeDisposable != null) {
            delayFaceTaskCompositeDisposable.clear();
        }

        if (faceHelperIr != null) {
            ConfigUtil.setTrackedFaceCount(this, faceHelperIr.getTrackedFaceCount());
            faceHelperIr.release();
            faceHelperIr = null;
        }

        //previewViewRgb.getViewTreeObserver().addOnGlobalLayoutListener(this);
        previewViewRgb.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        previewViewIr.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        if (mNirUVCCamera != null) {
            mNirUVCCamera.close();
            mNirUVCCamera.destroy();
            isNirActive = isNirPreview = false;
        }
        if (mUSBMonitor != null) {
            mUSBMonitor.unregister();
            mUSBMonitor.destroy();
        }
        if (mUVCCamera != null) {
            mUVCCamera.destroy();
            mUVCCamera = null;
        }


        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }

        FaceServer.getInstance().unInit();
        super.onDestroy();
    }


    /**
     * 处理预览数据
     */
    private synchronized void processPreviewData() {
        if (rgbData != null && irData != null) {
            final byte[] cloneNv21Rgb = rgbData.clone();
//            if (faceRectView != null) {
//                faceRectView.clearFaceInfo();
//            }
//            if (faceRectViewIr != null) {
//                faceRectViewIr.clearFaceInfo();
//            }
            List<FacePreviewInfo> facePreviewInfoList = faceHelperIr.onPreviewFrame(cloneNv21Rgb);
            if (facePreviewInfoList != null && faceRectView != null && drawHelperRgb != null) {
                drawPreviewInfo(facePreviewInfoList);
            }
            registerFace(cloneNv21Rgb, facePreviewInfoList);
            clearLeftFace(facePreviewInfoList);

            if (facePreviewInfoList != null && facePreviewInfoList.size() > 0 ) {
                for (int i = 0; i < facePreviewInfoList.size(); i++) {
                    // 注意：这里虽然使用的是IR画面活体检测，RGB画面特征提取，但是考虑到成像接近，所以只用了RGB画面的图像质量检测
                    Integer status = requestFeatureStatusMap.get(facePreviewInfoList.get(i).getTrackId());
                    /**
                     * 在活体检测开启，在人脸活体状态不为处理中（ANALYZING）且不为处理完成（ALIVE、NOT_ALIVE）时重新进行活体检测
                     */
                    if (livenessDetect && (status == null || status != RequestFeatureStatus.SUCCEED)) {
                        Integer liveness = livenessMap.get(facePreviewInfoList.get(i).getTrackId());
                        if (liveness == null
                                || (liveness != LivenessInfo.ALIVE && liveness != LivenessInfo.NOT_ALIVE && liveness != RequestLivenessStatus.ANALYZING)) {
                            livenessMap.put(facePreviewInfoList.get(i).getTrackId(), RequestLivenessStatus.ANALYZING);
                            // IR数据偏移
                            FaceInfo faceInfo = facePreviewInfoList.get(i).getFaceInfo().clone();
                            faceInfo.getRect().offset(Constants.HORIZONTAL_OFFSET, Constants.VERTICAL_OFFSET);
                            faceHelperIr.requestFaceLiveness(irData.clone(), faceInfo, UVCCamera.DEFAULT_PREVIEW_WIDTH,UVCCamera.DEFAULT_PREVIEW_HEIGHT, FaceEngine.CP_PAF_NV21, facePreviewInfoList.get(i).getTrackId(), LivenessType.IR);
                        }
                    }
                    /**
                     * 对于每个人脸，若状态为空或者为失败，则请求特征提取（可根据需要添加其他判断以限制特征提取次数），
                     * 特征提取回传的人脸特征结果在{@link FaceListener#onFaceFeatureInfoGet(FaceFeature, Integer, Integer)}中回传
                     */
                    if (status == null
                            || status == RequestFeatureStatus.TO_RETRY) {
                        requestFeatureStatusMap.put(facePreviewInfoList.get(i).getTrackId(), RequestFeatureStatus.SEARCHING);
                        faceHelperIr.requestFaceFeature(cloneNv21Rgb, facePreviewInfoList.get(i).getFaceInfo(),
                                UVCCamera.DEFAULT_PREVIEW_WIDTH,UVCCamera.DEFAULT_PREVIEW_HEIGHT, FaceEngine.CP_PAF_NV21,
                                facePreviewInfoList.get(i).getTrackId());
                    }
                }
            }
            rgbData = null;
            irData = null;
        }

    }

    /**
     * 绘制预览相关数据
     *
     * @param facePreviewInfoList {@link FaceHelper#onPreviewFrame(byte[])}回传的处理结果
     */
    private void drawPreviewInfo(List<FacePreviewInfo> facePreviewInfoList) {
        List<DrawInfo> drawInfoList = new ArrayList<>();
        for (int i = 0; i < facePreviewInfoList.size(); i++) {
            int trackId = facePreviewInfoList.get(i).getTrackId();
            String name = faceHelperIr.getName(trackId);
            Integer liveness = livenessMap.get(trackId);
            Rect ftRect = facePreviewInfoList.get(i).getFaceInfo().getRect();


            Integer recognizeStatus = requestFeatureStatusMap.get(facePreviewInfoList.get(i).getTrackId());

            // 根据识别结果和活体结果设置颜色
            int color = RecognizeColor.COLOR_UNKNOWN;
            if (recognizeStatus != null) {
                if (recognizeStatus == RequestFeatureStatus.FAILED) {
                    color = RecognizeColor.COLOR_FAILED;
                }
                if (recognizeStatus == RequestFeatureStatus.SUCCEED) {
                    color = RecognizeColor.COLOR_SUCCESS;
                }
            }
            if (liveness != null && liveness == LivenessInfo.NOT_ALIVE) {
                color = RecognizeColor.COLOR_FAILED;
            }
            drawInfoList.add(new DrawInfo(drawHelperRgb.adjustRect(ftRect),
                    GenderInfo.UNKNOWN, AgeInfo.UNKNOWN_AGE,
                    liveness != null ? liveness : LivenessInfo.UNKNOWN, color,
                    name == null ? String.valueOf(trackId) : name));

            Rect offsetFtRect = new Rect(ftRect);
            offsetFtRect.offset(Constants.HORIZONTAL_OFFSET, Constants.VERTICAL_OFFSET);
        }
        drawHelperRgb.draw(faceRectView, drawInfoList);
    }

    /**
     * 注册人脸
     *
     * @param nv21Rgb             RGB摄像头的帧数据
     * @param facePreviewInfoList {@link FaceHelper#onPreviewFrame(byte[])}回传的处理结果
     */
    private void registerFace(final byte[] nv21Rgb, final List<FacePreviewInfo> facePreviewInfoList) {
        if (registerStatus == REGISTER_STATUS_READY && facePreviewInfoList != null && facePreviewInfoList.size() > 0) {
            registerStatus = REGISTER_STATUS_PROCESSING;
            Observable.create(new ObservableOnSubscribe<Boolean>() {
                        @Override
                        public void subscribe(ObservableEmitter<Boolean> emitter) {
                            boolean success = FaceServer.getInstance().registerNv21(
                                    TestActivity.this, nv21Rgb,
                                    UVCCamera.DEFAULT_PREVIEW_WIDTH,UVCCamera.DEFAULT_PREVIEW_HEIGHT, facePreviewInfoList.get(0).getFaceInfo(), "学生： " + faceHelperIr.getTrackedFaceCount());
                            emitter.onNext(success);
                        }
                    })
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Boolean>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Boolean success) {
                            String result = success ? "注册成功!" : "注册失败!";
                            showToast(result);
                            registerStatus = REGISTER_STATUS_DONE;
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            showToast("register failed!");
                            registerStatus = REGISTER_STATUS_DONE;
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
    }

    @Override
    void afterRequestPermission(int requestCode, boolean isAllGranted) {
        if (requestCode == ACTION_REQUEST_PERMISSIONS) {
            if (isAllGranted) {
                initEngine();
            } else {
                showToast(getString(R.string.permission_denied));
            }
        }
    }


    /**
     * 删除已经离开的人脸
     *
     * @param facePreviewInfoList 人脸和trackId列表
     */
    private void clearLeftFace(List<FacePreviewInfo> facePreviewInfoList) {
        if (compareResultList != null) {
            for (int i = compareResultList.size() - 1; i >= 0; i--) {
                if (!requestFeatureStatusMap.containsKey(compareResultList.get(i).getTrackId())) {
                    compareResultList.remove(i);
                    dual_camera_view_person.setVisibility(View.GONE);
                }
            }
        }
        if (facePreviewInfoList == null || facePreviewInfoList.size() == 0) {
            requestFeatureStatusMap.clear();
            livenessMap.clear();
            livenessErrorRetryMap.clear();
            extractErrorRetryMap.clear();
            if (getFeatureDelayedDisposables != null) {
                getFeatureDelayedDisposables.clear();
            }
            return;
        }
        Enumeration<Integer> keys = requestFeatureStatusMap.keys();
        while (keys.hasMoreElements()) {
            int key = keys.nextElement();
            boolean contained = false;
            for (FacePreviewInfo facePreviewInfo : facePreviewInfoList) {
                if (facePreviewInfo.getTrackId() == key) {
                    contained = true;
                    break;
                }
            }
            if (!contained) {
                requestFeatureStatusMap.remove(key);
                livenessMap.remove(key);
                livenessErrorRetryMap.remove(key);
                extractErrorRetryMap.remove(key);
            }
        }


    }

    private void searchFace(final FaceFeature frFace, final Integer requestId) {
        Observable
                .create(new ObservableOnSubscribe<CompareResult>() {
                    @Override
                    public void subscribe(ObservableEmitter<CompareResult> emitter) {
//                        Log.i(TAG, "subscribe: fr search start = " + System.currentTimeMillis() + " trackId = " + requestId);
                        CompareResult compareResult = FaceServer.getInstance().getTopOfFaceLib(frFace);
//                        Log.i(TAG, "subscribe: fr search end = " + System.currentTimeMillis() + " trackId = " + requestId);
                        emitter.onNext(compareResult);

                    }
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CompareResult>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(CompareResult compareResult) {
                        if (compareResult == null || compareResult.getUserName() == null) {
                            requestFeatureStatusMap.put(requestId, RequestFeatureStatus.FAILED);
                            faceHelperIr.setName(requestId, "VISITOR " + requestId);
                            return;
                        }

//                        Log.i(TAG, "onNext: fr search get result  = " + System.currentTimeMillis() + " trackId = " + requestId + "  similar = " + compareResult.getSimilar());
                        if (compareResult.getSimilar() > SIMILAR_THRESHOLD) {
                            boolean isAdded = false;
                            if (compareResultList == null) {
                                requestFeatureStatusMap.put(requestId, RequestFeatureStatus.FAILED);
                                faceHelperIr.setName(requestId, "VISITOR " + requestId);
                                return;
                            }
                            for (CompareResult compareResult1 : compareResultList) {
                                if (compareResult1.getTrackId() == requestId) {
                                    isAdded = true;
                                    break;
                                }
                            }
                            if (!isAdded) {
                                //对于多人脸搜索，假如最大显示数量为 MAX_DETECT_NUM 且有新的人脸进入，则以队列的形式移除
                                if (compareResultList.size() >= MAX_DETECT_NUM) {
                                    compareResultList.remove(0);
                                    dual_camera_view_person.setVisibility(View.GONE);
                                }
                                //添加显示人员时，保存其trackId
                                compareResult.setTrackId(requestId);
                                compareResultList.add(compareResult);
                            }
                            requestFeatureStatusMap.put(requestId, RequestFeatureStatus.SUCCEED);
                            faceHelperIr.setName(requestId, getString(R.string.recognize_success_notice, compareResult.getUserName()));

                            // todo


                            dual_camera_view_person.setVisibility(View.VISIBLE);
                            File imgFile = new File(FaceServer.ROOT_PATH + File.separator + FaceServer.SAVE_IMG_DIR + File.separator + compareResultList.get(0).getUserName() + FaceServer.IMG_SUFFIX);
                            Glide.with(iv_item_head_img)
                                    .load(imgFile)
                                    .into(iv_item_head_img);
                            tv_item_name.setText(compareResultList.get(0).getUserName());

                            payUser = new UserEntity();
                            payUser.setCard(compareResult.getTrackId()+"");
                            payUser.setName(compareResult.getUserName());
                            /*[人脸检测通过]*/
                            mHandler.obtainMessage(HANDLER_YM_FACE_ANALYZE_SUCCESS, payUser).sendToTarget();

                        } else {
                            faceHelperIr.setName(requestId, getString(R.string.recognize_failed_notice, "未注册"));
                            retryRecognizeDelayed(requestId);
                            retryLivenessDetectDelayed2(requestId);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        faceHelperIr.setName(requestId, getString(R.string.recognize_failed_notice, "未注册"));
                        retryRecognizeDelayed(requestId);
                        retryLivenessDetectDelayed2(requestId);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
    private void playMediaPlayer(int type) {
        try {
            Uri uri;
            String packageName = getPackageName();
            if (type == 1) {
                uri = Uri.parse("android.resource://" + packageName + "/raw/" + R.raw.check_success);
            } else if (type == 2) {
                uri = Uri.parse("android.resource://" + packageName + "/raw/" + R.raw.check_cancel);
            } else if (type == 4) {
                uri = Uri.parse("android.resource://" + packageName + "/raw/" + R.raw.check_again);
            }else if (type == 5) {
                uri = Uri.parse("android.resource://" + packageName + "/raw/" + R.raw.notfund);
            }else {
                uri = Uri.parse("android.resource://" + packageName + "/raw/" + R.raw.check_fail);
            }
            Log.d("CHECK_AUDIO", " -- create media player...");
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(this, uri);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);//音量跟随闹钟音量
//            mediaPlayer.prepare();
//            mediaPlayer.start();
//            mediaPlayer.release();

            mediaPlayer.prepareAsync();
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    // 装载完毕回调
                    Log.d("CHECK_AUDIO", " -- start media player...");
                    mediaPlayer.start();
                }
            });

            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    // 在播放完毕被回调
                    if (mediaPlayer != null) {
                        Log.d("CHECK_AUDIO", " -- release media player.");
                        mediaPlayer.stop();
                        mediaPlayer.release();
                        mediaPlayer = null;
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 将准备注册的状态置为{@link #REGISTER_STATUS_READY}
     *
     * @param view 注册按钮
     */
    public void register(View view) {
        if (registerStatus == REGISTER_STATUS_DONE) {
            registerStatus = REGISTER_STATUS_READY;
        }
    }

    /**
     * 在{@link #previewViewRgb}第一次布局完成后，去除该监听，并且进行引擎和相机的初始化
     */
    @Override
    public void onGlobalLayout() {
        previewViewRgb.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        if (!checkPermissions(NEEDED_PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, NEEDED_PERMISSIONS, ACTION_REQUEST_PERMISSIONS);
        } else {
            initEngine();
        }
    }

    public void drawIrRectVerticalMirror(View view) {
        if (drawHelperIr != null) {
            drawHelperIr.setMirrorVertical(!drawHelperIr.isMirrorVertical());
        }
    }

    public void drawIrRectHorizontalMirror(View view) {
        if (drawHelperIr != null) {
            drawHelperIr.setMirrorHorizontal(!drawHelperIr.isMirrorHorizontal());
        }
    }


    /**
     * 将map中key对应的value增1回传
     *
     * @param countMap map
     * @param key      key
     * @return 增1后的value
     */
    public int increaseAndGetValue(Map<Integer, Integer> countMap, int key) {
        if (countMap == null) {
            return 0;
        }
        Integer value = countMap.get(key);
        if (value == null) {
            value = 0;
        }
        countMap.put(key, ++value);
        return value;
    }

    /**
     * 延迟 FAIL_RETRY_INTERVAL 重新进行活体检测
     *
     * @param requestId 人脸ID
     */
    private void retryLivenessDetectDelayed(final Integer requestId) {
        Observable.timer(FAIL_RETRY_INTERVAL, TimeUnit.MILLISECONDS)
                .subscribe(new Observer<Long>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                        delayFaceTaskCompositeDisposable.add(disposable);
                    }

                    @Override
                    public void onNext(Long aLong) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        // 将该人脸状态置为UNKNOWN，帧回调处理时会重新进行活体检测
                        if (livenessDetect) {
                            faceHelperIr.setName(requestId, "未知:"+requestId);
                        }
                        livenessMap.put(requestId, LivenessInfo.UNKNOWN);
                        delayFaceTaskCompositeDisposable.remove(disposable);
                    }
                });
    }
    private Map<Integer, Long> lastExecutionTimes = new HashMap<>();

    private void retryLivenessDetectDelayed2(final Integer requestId) {
        long currentTime = System.currentTimeMillis();
        long lastRetryTime = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            lastRetryTime = lastExecutionTimes.getOrDefault(requestId, currentTime);
        }
        if (currentTime - lastRetryTime >= 3000) {
            // 距离上次重试已经过去了5秒或更多，执行操作
            // 执行您的操作
            lastExecutionTimes.remove(requestId);
            playMediaPlayer(5);
        } else {
            // 5秒内的重试，不执行操作
            lastExecutionTimes.put(requestId, lastRetryTime);
        }
    }



    /**
     * 延迟 FAIL_RETRY_INTERVAL 重新进行人脸识别
     *
     * @param requestId 人脸ID
     */
    private void retryRecognizeDelayed(final Integer requestId) {
        Log.i(TAG, "retryRecognizeDelayed: " + requestId);
        requestFeatureStatusMap.put(requestId, RequestFeatureStatus.FAILED);
        Observable.timer(FAIL_RETRY_INTERVAL, TimeUnit.MILLISECONDS)
                .subscribe(new Observer<Long>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                        delayFaceTaskCompositeDisposable.add(disposable);
                        Log.i(TAG, "onSubscribe: " + requestId);
                    }

                    @Override
                    public void onNext(Long aLong) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        Log.i(TAG, "onComplete: " + requestId);
                        // 将该人脸特征提取状态置为FAILED，帧回调处理时会重新进行活体检测
                        faceHelperIr.setName(requestId, "未知:"+requestId);
                        requestFeatureStatusMap.put(requestId, RequestFeatureStatus.TO_RETRY);
                        delayFaceTaskCompositeDisposable.remove(disposable);
                    }
                });
    }




    final FaceListener faceListener = new FaceListener() {
        @Override
        public void onFail(Exception e) {
            Log.e(TAG, "onFail: " + e.getMessage());
        }

        //请求FR的回调
        @Override
        public void onFaceFeatureInfoGet(@Nullable final FaceFeature faceFeature, final Integer requestId, final Integer errorCode) {
            //FR成功
            if (faceFeature != null) {
//                    Log.i(TAG, "onPreview: fr end = " + System.currentTimeMillis() + " trackId = " + requestId);
                Integer liveness = livenessMap.get(requestId);
                //不做活体检测的情况，直接搜索
                if (!livenessDetect) {
                    searchFace(faceFeature, requestId);
                }
                //活体检测通过，搜索特征
                else if (liveness != null && liveness == LivenessInfo.ALIVE) {
                    searchFace(faceFeature, requestId);
                }
                //活体检测未出结果，或者非活体，延迟执行该函数
                else {

                    if (requestFeatureStatusMap.containsKey(requestId)) {
                        Observable.timer(WAIT_LIVENESS_INTERVAL, TimeUnit.MILLISECONDS)
                                .subscribe(new Observer<Long>() {
                                    Disposable disposable;

                                    @Override
                                    public void onSubscribe(Disposable d) {
                                        disposable = d;
                                        getFeatureDelayedDisposables.add(disposable);
                                    }

                                    @Override
                                    public void onNext(Long aLong) {
                                        onFaceFeatureInfoGet(faceFeature, requestId, errorCode);
                                    }

                                    @Override
                                    public void onError(Throwable e) {

                                    }

                                    @Override
                                    public void onComplete() {
                                        getFeatureDelayedDisposables.remove(disposable);
                                    }
                                });
                    }
                }

            }
            //特征提取失败
            else {
                if (increaseAndGetValue(extractErrorRetryMap, requestId) > MAX_RETRY_TIME) {
                    extractErrorRetryMap.put(requestId, 0);
                    String msg;
                    // 传入的FaceInfo在指定的图像上无法解析人脸，此处使用的是RGB人脸数据，一般是人脸模糊
                    if (errorCode != null && errorCode == ErrorInfo.MERR_FSDK_FACEFEATURE_LOW_CONFIDENCE_LEVEL) {
                        msg = getString(R.string.low_confidence_level);
                    } else {
                        msg = "ExtractCode:" + errorCode;
                    }
                    faceHelperIr.setName(requestId, getString(R.string.recognize_failed_notice, msg));
                    // 在尝试最大次数后，特征提取仍然失败，则认为识别未通过
                    requestFeatureStatusMap.put(requestId, RequestFeatureStatus.FAILED);
                    retryRecognizeDelayed(requestId);
                } else {
                    requestFeatureStatusMap.put(requestId, RequestFeatureStatus.TO_RETRY);
                }
            }
        }

        @Override
        public void onFaceLivenessInfoGet(@Nullable LivenessInfo livenessInfo, final Integer requestId, Integer errorCode) {
            if (livenessInfo != null) {
                int liveness = livenessInfo.getLiveness();
                livenessMap.put(requestId, liveness);
                // 非活体，重试
                if (liveness == LivenessInfo.NOT_ALIVE) {
                    faceHelperIr.setName(requestId, getString(R.string.recognize_failed_notice, "NOT_ALIVE"));
                    // 延迟 FAIL_RETRY_INTERVAL 后，将该人脸状态置为UNKNOWN，帧回调处理时会重新进行活体检测
                    retryLivenessDetectDelayed(requestId);
                }
            } else {
                if (increaseAndGetValue(livenessErrorRetryMap, requestId) > MAX_RETRY_TIME) {
                    livenessErrorRetryMap.put(requestId, 0);
                    String msg;
                    // 传入的FaceInfo在指定的图像上无法解析人脸，此处使用RGB人脸框 + IR数据，一般是人脸模糊或画面中无人脸
                    if (errorCode != null && errorCode == ErrorInfo.MERR_FSDK_FACEFEATURE_LOW_CONFIDENCE_LEVEL) {
                        msg = getString(R.string.low_confidence_level);
                    } else {
                        msg = "ProcessCode:" + errorCode;
                    }
                    faceHelperIr.setName(requestId, getString(R.string.recognize_failed_notice, msg));
                    // 在尝试最大次数后，活体检测仍然失败，则认定为非活体
                    livenessMap.put(requestId, LivenessInfo.NOT_ALIVE);
                    retryLivenessDetectDelayed(requestId);
                } else {
                    livenessMap.put(requestId, LivenessInfo.UNKNOWN);
                }
            }
        }

    };

//    @Override
//    protected void onCameraConnected(@NonNull MultiCameraClient.ICamera iCamera) {
//        Log.d(TAG, "onCameraConnected: " + iCamera);
//        MultiCameraClient.ICamera camera;
//        for (int i = 0; i < mCameraList.size(); i++) {
//            camera = mCameraList.get(i);
//            if (camera.getUsbDevice().getDeviceId() == iCamera.getUsbDevice().getDeviceId()) {
//                MultiCameraClient.ICamera finalCamera = camera;
//                CameraRequest request = getCameraRequest();
//                // todo 这里可以根据deviceId来确定是ir还是rgb，这个id是固定的，你自己修改。
//                String productName ="";
//                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//                    productName = iCamera.getUsbDevice().getProductName();
//                }
//                if (!productName.equals("NIR Camera")) {
//                    List<PreviewSize> allPreviewSizes = iCamera.getAllPreviewSizes(null);
//                    Log.i(TAG, "onCameraConnected: " + allPreviewSizes);
//                    previewSize = request;
//                    previewViewRgb.post(() -> {
//                        finalCamera.openCamera(previewViewRgb, request);
//                        //todo 根据实际情况具体修改
//                        drawHelperRgb = new DrawHelper(previewSize.getPreviewWidth(), previewSize.getPreviewHeight(), previewViewRgb.getWidth(),
//                                previewViewRgb.getHeight(), 0,
//                                0, false, true, false);
//                        if (faceHelperIr == null) {
//                            faceHelperIr = new FaceHelper.Builder()
//                                    .ftEngine(ftEngine)
//                                    .frEngine(frEngine)
//                                    .flEngine(flEngine)
//                                    .frQueueSize(MAX_DETECT_NUM)
//                                    .flQueueSize(MAX_DETECT_NUM)
//                                    .previewPair(new Pair<>(previewSize.getPreviewWidth(), previewSize.getPreviewHeight()))
//                                    .faceListener(faceListener)
//                                    .trackedFaceCount(ConfigUtil.getTrackedFaceCount(TestActivity.this.getApplicationContext()))
//                                    .build();
//                        }
//                    });
//                    camera.addPreviewDataCallBack((bytes, i12, i1, dataFormat) -> {
//                        rgbData = bytes;
//                        processPreviewData();
//                    });
//                } else {
//                    List<PreviewSize> allPreviewSizes = iCamera.getAllPreviewSizes(null);
//                    Log.i(TAG, "onCameraConnected: " + allPreviewSizes);
//                    previewSizeIr = request;
//                    previewViewIr.post(() -> {
//                        finalCamera.openCamera(previewViewIr, request);
//                        //根据实际情况具体修改
//                        drawHelperIr = new DrawHelper(previewSizeIr.getPreviewWidth(), previewSizeIr.getPreviewHeight(),
//                                previewViewIr.getWidth(), previewViewIr.getHeight(), 0,
//                                1, false, true, false);
//                    });
//                    camera.addPreviewDataCallBack((bytes, i13, i1, dataFormat) -> {
//                        irData = bytes;
//                    });
//                }
//                camera.setCameraStateCallBack(this);
//                break;
//            }
//        }
//        for (int i = 0; i < mCameraList.size(); i++) {
//            camera = mCameraList.get(i);
//            UsbDevice device = camera.getUsbDevice();
//            if (!hasPermission(device)) {
//                requestPermission(device);
//            }
//        }
//
//    }


}
