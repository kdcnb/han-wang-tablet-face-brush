package com.arcsoft.arcfacedemo.common;

public class Constants {

    public static final String APP_ID = "58rUbTFvUSNNLAPw23aQdZD7aCEPa9z5pMokWnjGyokm";
    public static final String SDK_KEY = "4HPw4amXGPff3n6oRo3mB1QrAZor8aMYjdqcnRMEbiZ6";

    /**
     * IR预览数据相对于RGB预览数据的横向偏移量，注意：是预览数据，一般的摄像头的预览数据都是 width > height
     */
    public static final int HORIZONTAL_OFFSET = 0;
    /**
     * IR预览数据相对于RGB预览数据的纵向偏移量，注意：是预览数据，一般的摄像头的预览数据都是 width > height
     */
    public static final int VERTICAL_OFFSET = 0;
    public static final int SAME_USER_COST_GAP = 1000 * 5;
}

