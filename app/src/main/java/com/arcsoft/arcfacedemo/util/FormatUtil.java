package com.arcsoft.arcfacedemo.util;

import java.nio.ByteBuffer;

public class FormatUtil {

    //String转float
    public static float[] stringToFloat(String string) {
        String[] strings = string.split(",");
        float[] floats = new float[strings.length];
        for (int i = 0; i < strings.length; i++) {
            floats[i] = Float.parseFloat(strings[i]);
        }
        return floats;
    }

    //float[]转String float[] fs = { 1.83f, 2.50f, 3.14f } => 1.83,2.5, 3.14
    public static String floatToString(float[] fs) {
        StringBuilder sb = new StringBuilder();
        sb.append(fs[0]);
        for (int i = 1; i < fs.length; i++) {
            sb.append("," + fs[i]);
        }
        String s = sb.toString();
        return s;
    }


    /**
     * byteBuffer 转 byte数组
     * @param buffer
     * @return
     */
    public static byte[] bytebuffer2ByteArray(ByteBuffer buffer) {
        //重置 limit 和postion 值
        buffer.flip();
        //获取buffer中有效大小
        int len=buffer.limit() - buffer.position();

        byte [] bytes=new byte[len];

        for (int i = 0; i < bytes.length; i++) {
            bytes[i]=buffer.get();

        }

        return bytes;
    }
}
